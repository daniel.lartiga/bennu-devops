# bennu-devops

## Crear EC2 con Terraform

Setear variables de entorno `AWS_ACCESS_KEY_ID` y `AWS_SECRET_ACCESS_KEY`
```shell
export AWS_ACCESS_KEY_ID=xxxxxx
export AWS_SECRET_ACCESS_KEY=xxxxxx
```

Provisionar VM con Terraform
```shell
terraform init
terraform validate
terraform plan --out=planfile
terraform apply "planfile"
```

Ver domain name de VM creada
```shell
terraform output "public_dns"
```

## Provisionar docker-engine con Ansible

Usando [este rol](https://github.com/nickjj/ansible-docker)
```shell
ansible-galaxy install nickjj.docker
```

Agregar VM a `/etc/ansible/hosts`
```shell
terraform output "public_dns" >> /etc/ansible/hosts
```

Ejecutar playbook
```shell
ansible-playbook install ./ansible/docker.yml
```

## Servicio Hello World

Constuir imagen
```shell
docker build -t dlartigae/python-hello-world:latest ./service
```

o desde [Docker Hub](https://hub.docker.com/r/dlartigae/python-hello-world)
```shell
docker pull dlartigae/python-hello-world:latest
```

Ejecutar contenedor
```shell
docker run -d --name server -p 80:80 dlartigae/python-hello-world:latest
```

Testear servicio con curl
```shellell
curl "http://localhost:80" -v
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 80 (#0)
> GET / HTTP/1.1
> Host: localhost
> User-Agent: curl/7.58.0
> Accept: */*
> 
* HTTP 1.0, assume close after body
< HTTP/1.0 200 OK
< Server: BaseHTTP/0.6 Python/3.7.6
< Date: Tue, 31 Dec 2019 23:10:12 GMT
< Content-type: text/plain
< 
* Closing connection 0
Hello World!!
```

## Kong

Desplegar kong como contenedor y agregar API de servicio hello-world usando Ansible
```shell
ansible-playbook ansible/helloworld-service.yml
```

Testear servicio hello-world
```shell
curl "http://ec2-18-213-218-178.compute-1.amazonaws.com:8000" --header "Host: helloworld.dlartigae.com" -v
* Rebuilt URL to: http://ec2-18-213-218-178.compute-1.amazonaws.com:8000/
*   Trying 18.213.218.178...
* TCP_NODELAY set
* Connected to ec2-18-213-218-178.compute-1.amazonaws.com (18.213.218.178) port 8000 (#0)
> GET / HTTP/1.1
> Host: helloworld.dlartigae.com
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< Content-Type: text/plain; charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< Server: BaseHTTP/0.6 Python/3.7.6
< Date: Thu, 02 Jan 2020 00:13:03 GMT
< X-Kong-Upstream-Latency: 1
< X-Kong-Proxy-Latency: 1
< Via: kong/1.4.2
< 
* Connection #0 to host ec2-18-213-218-178.compute-1.amazonaws.com left intact
Hello World!!
```

## Script

Ejecutar todo
```shell
./run.sh
```

## Challenges

### Missing

```shell
docker run -it --rm dlartigae/bennu-jobs:missing-fixed
```
