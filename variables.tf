variable "aws_region" {
  description = "AWS region"
  default = "us-east-1"
}

variable "ami_id" {
  description = "ID of the AMI to provision. Default is Debian Stretch"
  default = "ami-05f27d4d6770a43d2"
}

variable "instance_type" {
  description = "type of EC2 instance to provision."
  default = "t2.micro"
}

variable "name" {
  description = "name to pass to Name tag"
  default = "Debian 9"
}

variable "key_name" {
  description = "ssh deployment key name"
  default = "deployer"
}
