#!/usr/bin/env bash


function check_dependency {
    echo -e "Checkando version de $1\n"
    if ! which $1 ; then
        echo -e "ERROR: No se ha encontrado $1 en \$PATH=$PATH\n\n"
        exit 1
    else
        $1 --version
        echo -e "\n\n"
    fi
}

function create_vm {
    echo -e "Creando VM con teerraform\n"
    if [[ -z "$AWS_ACCESS_KEY_ID" || -z "$AWS_SECRET_ACCESS_KEY" ]]; then
        echo -e "ERROR: Variables AWS_ACCESS_KEY_ID y AWS_SECRET_ACCESS_KEY deben estar seteadas\n\n"
        exit 1
    else
        # traer .terraform y terraform.tfstate desde S3
        terraform init
        terraform plan --out=planfile
        terraform apply "planfile"
        terraform output "public_dns" > .ec2_public_dns
        # mover .terraform y terraform.tfstate a S3
    fi
}

function provision_services {
    if [ -f ~/.ssh/deployer.pem ]; then
        echo -e "Provisionando servicio con Ansible\n"
        echo debian ansible_host=$(cat .ec2_public_dns) >> /etc/ansible/hosts
        ansible-galaxy install nickjj.docker
        ansible-playbook ansible/docker.yml
        ansible-playbook ansible/helloworld-service.yml
    else
        echo -e "ERROR: No se encuentra llave privada ~/.ssh/deployer.pem\n\n"
        exit 1
    fi
}

function test {
    res=$(curl http://$(cat .ec2_public_dns):8000 --header "host: helloworld.dlartigae.com")
    if [[ $res == "Hello World!!" ]]; then
        echo $res;
    else
        echo -e "ERROR: respuesta inesperada $res"
        exit 1
    fi
}


# check dependencies
for dependency in terraform ansible; do 
    check_dependency $dependency
done

# create VM
create_vm

# provision
provision_services

# test service
test