from http.server import BaseHTTPRequestHandler, HTTPServer


PORT = 80
response = "Hello World!!"

class ServerHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()
        self.wfile.write(response.encode(encoding="utf-8", errors="strict"))
        return None


if __name__ == "__main__":
    try:
        server = HTTPServer(("", PORT), ServerHandler)
        print("Starting Server on port {}".format(PORT))
        server.serve_forever()
    except BaseException as e:
        print("Server got unhandled exception '{}: {}',\nClosing server...".
            format(type(e).__name__, e.__doc__)
        )
        server.socket.close()
