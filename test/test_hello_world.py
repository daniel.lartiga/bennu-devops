import requests
import os


PORT = 8000
URL = 'http://{}:{}'.format(os.environ["EC2_PUBLIC_DNS"], PORT)


def test():
    r = requests.get(URL, headers={'host': 'helloworld.dlartigae.com'})

    assert r.status_code == 200
    assert r.text == "Hello World!!"